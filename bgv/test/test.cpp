#include <lhe.h>
#include <iostream>
#include <NTL/ZZ.h>

using namespace std;
using namespace NTL;

#define NUM 100
#define N 240
#define Q 79
#define D 128

void rheTest(){
  int count=0, ok=0, nok=0;
  rhe *scheme;
  double delta = 1;
  long n = N;
  long d = D;
  ZZ q;
  GenPrime(q, Q);
  ZZ_p::init(conv<ZZ>(q)); 
  ZZ_pX P;
  SetCoeff(P, 0, 1);
  SetCoeff(P, d, 1);
  ZZ_pE::init(P); 
  cout << "BGV Homomorphic encryption test:\n";
  scheme = new rhe();
  scheme->ParamsGen(n, d, q, delta);
  
  while(count < NUM){
    scheme->KeyGen();
    ZZ_pE m = scheme->SampleMessage();
    cout << ">" << m << "\n";
    Vec<ZZ_pE> c1 = scheme->Encrypt(m);
    ZZ_pE mm = scheme->Decrypt(c1);
    cout << "###antes: " << m << "," << m.degree() << ";\n";
    cout << "###depois: " << mm << "," << mm.degree() << ";\n";
    if(m == mm)
      ok++;
    else
      nok++;
    count++;
  }
  cout << "ok,nok: " << ok << "," << nok << "\n";
}

void rheAddTest(){
  int count=0, ok=0, nok=0;
  rhe *scheme;
  double delta = 1;
  long n = N;
  long d = D;
  ZZ q;
  GenPrime(q, Q);
  ZZ_p::init(conv<ZZ>(q)); 
  ZZ_pX P;
  SetCoeff(P, 0, 1);
  SetCoeff(P, d, 1);
  ZZ_pE::init(P); 
  cout << "BGV Homomorphic encryption test:\n";
  scheme = new rhe();
  scheme->ParamsGen(n, d, q, delta);
  
  while(count < NUM){
    scheme->KeyGen();
    ZZ_pE m1 = scheme->SampleMessage();
    ZZ_pE m2 = scheme->SampleMessage();
    cout << ">" << m1 << "\n";
    cout << ">" << m2 << "\n";
    Vec<ZZ_pE> c1 = scheme->Encrypt(m1);
    Vec<ZZ_pE> c2 = scheme->Encrypt(m2);
    ZZ_pE mm = scheme->Decrypt(c1+c2);
    if(scheme->Mod2(m1+m2) == mm)
      ok++;
    else
      nok++;
    count++;
  }
  cout << "ok,nok: " << ok << "," << nok << "\n";
}

/*void lheMulTest(){
  int count=0, ok=0, nok=0;
  long lwq = 10;
  double delta = 1;
  long n = N;
  long t = T;
  lhe *fnord;
  //ZZ q = conv<ZZ>(16381);
  ZZ q;
  GenPrime(q, Q);
  ZZ_p::init(conv<ZZ>(q));
  ZZ_pX P, pol;
  SetCoeff(P, 0, 1);
  SetCoeff(P, n, 1);
  ZZ_pE::init(P); 
  cout << "Leveled homomorphic encryption test:\n";
  fnord = new lhe();
  fnord->ParamsGen(t, n, q, delta, lwq);
  while(count < 100){
    fnord->KeyGen();
    long m1 = fnord->b->SampleMessage();
    long m2 = fnord->b->SampleMessage();
    cout << "m1: " << m1 << ", m2:" << m2 << "\n";
    ZZ_pE c1 = fnord->Encrypt(m1);
    //cout << "c1: " << c1 << "\n";
    ZZ_pE c2 = fnord->Encrypt(m2);
    //cout << "c2: " << c2 << "\n";
    //long mm = fnord->Decrypt(fnord->KeySwitch(fnord->Mult(c1,c2,q)));
    //cout << "mm: " << mm << "\n";
    //if ((m1*m2) == mm)
    //  ok++;
    //else
    //  nok++;
    count++;
  }
  cout << "ok,nok: " << ok << "," << nok << "\n";
}

void lheAddTest(){
  int count=0, ok=0, nok=0;
  long lwq = 10;
  double delta = 1;
  long n = N;
  long t = T;
  lhe *fnord;
  ZZ q ;
  GenPrime(q, Q);
  ZZ_p::init(q);
  ZZ_pX P;
  SetCoeff(P, 0, 1);
  SetCoeff(P, n, 1);
  ZZ_pE::init(P); 
  cout << "Leveled homomorphic encryption test:\n";
  fnord = new lhe();
  fnord->ParamsGen(t, n, q, delta, lwq);
  while(count < NUM){
    fnord->KeyGen();
    long m1 = fnord->b->SampleMessage();
    long m2 = fnord->b->SampleMessage();
    ZZ_pE c1 = fnord->Encrypt(m1);
    ZZ_pE c2 = fnord->Encrypt(m2);
    long mm = fnord->Decrypt(c1+c2);
    if ((m1+m2)%2 == mm)
      ok++;
    else
      nok++;
    count++;
  }
  cout << "ok,nok: " << ok << "," << nok << "\n";
}

void lheTest(){
  int count=0, ok=0, nok=0;
  long lwq = 10;
  double delta = 1;
  long n = N;
  long t = T;
  lhe *fnord;
  ZZ q;
  GenPrime(q, Q);
  ZZ_p::init(conv<ZZ>(q));
  ZZ_pX P;
  SetCoeff(P, 0, 1);
  SetCoeff(P, n, 1);
  ZZ_pE::init(P); 
  cout << "Leveled homomorphic encryption test:\n";
  fnord = new lhe();
  fnord->ParamsGen(t, n, q, delta, lwq);
  while(count < NUM){
    fnord->KeyGen();
    long m = fnord->b->SampleMessage();
    ZZ_pE c1 = fnord->Encrypt(m);
    long mm = fnord->Decrypt(c1);
    if (m == mm)
      ok++;
    else
      nok++;
    count++;
  }
  cout << "ok,nok: " << ok << "," << nok << "\n";
}*/

int main(){
  srand (time(NULL));
  //rheTest();
  rheAddTest();
  //lheTest();
  //lheAddTest();
  //lheMulTest();
  return 0;
}
