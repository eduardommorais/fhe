#include <lhe.h>
#include <math.h>
#include <stdio.h>
#include <NTL/ZZX.h>

using namespace std;

lhe::lhe(){
  this->b = new rhe();
}


Vec<ZZ_pE> lhe::GetKeySwitch(){
  return this->gamma;
}

Vec<ZZ_pE> lhe::KeySwitchGen(){
  int i;
  Vec<ZZ_pE> e, s;
  ZZ_pE h = this->b->PublicKey();
  s.SetLength(this->lwq);
  e = this->PowerOf(this->b->PrivateKey());
  for(i=0;i<this->lwq;i++){
    e[i] += this->b->SampleErr();
    s[i] = this->b->SampleErr();
    s[i] *= h;
  }
  return e+s;
}

void lhe::KeyGen(){
  this->b->KeyGen();
  this->gamma = this->KeySwitchGen();
}

void lhe::ParamsGen(long t, long n, ZZ q, double delta, long lwq){	
  this->lwq = lwq;
  this->b->ParamsGen(t, n, q, delta);
}

Vec<ZZ_pE> lhe::BitDecomp(ZZ_pE x){
  int i, j;
  Vec<ZZ_pE> res;
  res.SetLength(this->lwq);
  ZZ w = power(conv<ZZ>(2), 2);
  ZZ_pX pol = conv<ZZ_pX>(x);
  ZZ_pX aux;
  for(i=0; i<this->lwq; i++){
    for (j=0; j<=deg(pol);j++){
      ZZ c = conv<ZZ>(coeff(pol, j));
      SetCoeff(aux, j, conv<ZZ_p>(c%w));
      SetCoeff(pol, j, conv<ZZ_p>(c/w));
    }
    res[i] = conv<ZZ_pE>(aux);
  }
  return res;
}

Vec<ZZ_pE> lhe::PowerOf(ZZ_pE x){
  int i, j;
  Vec<ZZ_pE> res;
  res.SetLength(this->lwq);
  ZZ w = power(conv<ZZ>(2), 2);
  ZZ k = conv<ZZ>(1);
  ZZ_pX pol = conv<ZZ_pX>(x);
  ZZ_pX aux;
  for(i=0; i<this->lwq; i++){
    for (j=0; j<=deg(pol);j++){
      ZZ c = conv<ZZ>(coeff(pol, j));
      SetCoeff(aux, j, conv<ZZ_p>(c));
      SetCoeff(pol, j, conv<ZZ_p>(c*w));
    }
    res[i] = conv<ZZ_pE>(aux);
  }
  return res;
}

ZZ_pE lhe::Encrypt(long m){
  return this->b->Encrypt(m);
}

long lhe::Decrypt(ZZ_pE c){
  return this->b->Decrypt(c);
}
  
ZZ_pE lhe::InnerProduct(Vec<ZZ_pE> a, Vec<ZZ_pE> b){
  int i;
  ZZ_pE res = conv<ZZ_pE>(0);
  for(i=0;i<a.length();i++){
    res += a[i]*b[i];
  }
  return res;
}

ZZ_pE lhe::KeySwitch(ZZ_pE c){
  ZZ_pE res;
  Vec<ZZ_pE> dqw = this->BitDecomp(c);
  res = InnerProduct(dqw, this->gamma);
  res = this->b->CenterLift(res);
  return res;
}
    
ZZX lhe::Mult(ZZ_pE a, ZZ_pE b, ZZ q){
  int i;
  ZZ_pEBak bak;
  bak.save();  
  ZZ qq = 8*q*q+1;
  ZZ_p::init(conv<ZZ>(qq));
  ZZ_pX P, pol;
  SetCoeff(P, 0, 1);
  SetCoeff(P, this->b->n, 1);
  ZZ_pE::init(P);

  ZZ_pE c = a*b;
  //cout << "c: " << c << "\n";
  pol = conv<ZZ_pX>(c);
  ZZX ppol = conv<ZZX>(pol);
  for(i=0;i<=this->b->n;i++){
      ZZ aux = conv<ZZ>(coeff(pol, i));
      if (2*aux % q > q/2)
      	aux = 2*aux/q + 1;
      else 
	aux = 2*aux/q;
      if (aux % q > q/2){
	aux = (aux % q);
        aux -= q;
      }
      else
	aux = aux % q;
      SetCoeff(ppol, i, aux);
  }
  cout << "ppol: " << ppol << "\n";
  return ppol;
  //pol = conv<ZZ_pX>(ppol);
  //c = conv<ZZ_pE>(pol);
  //cout << "c: " << c << "\n";
  //bak.restore();
  //ZZ_p::init(conv<ZZ>(q));
  //SetCoeff(P, 0, 1);
  //SetCoeff(P, this->b->n, 1);
  //ZZ_pE::init(P);


  /*ZZX pa; 
  ZZX pb; 
  
  ZZ_p auxp = coeff(pola, 0);
  ZZ aux = conv<ZZ>(auxp);
  auxp = coeff(polb, 0);
  ZZ aux2 = conv<ZZ>(auxp);
  aux = aux*aux2;
  auxp = coeff(pola, 1);
  aux2 = conv<ZZ>(auxp);
  auxp = coeff(polb, 1);
  ZZ aux3 = conv<ZZ>(auxp);
  aux -= aux3*aux2;
  aux *= this->b->t;
  aux /= q;
  aux %= q;
  SetCoeff(pa, 0, aux);

  auxp = coeff(pola, 0);
  aux = conv<ZZ>(auxp);
  auxp = coeff(polb, 1);
  aux2 = conv<ZZ>(auxp);
  aux = aux*aux2;
  auxp = coeff(pola, 1);
  aux2 = conv<ZZ>(auxp);
  auxp = coeff(polb, 0);
  aux3 = conv<ZZ>(auxp);
  aux += aux3*aux2;
  aux *= this->b->t;
  aux /= q;
  aux %= q;
  SetCoeff(pa, 1, aux);
  cout << "pol: " << pa << "\n";
  
  pola = conv<ZZ_pX>(pa);
  ZZ_pE res = conv<ZZ_pE>(pola);
  res = this->b->CenterLift(res);*/
  return c;
}
