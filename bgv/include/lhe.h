#include <rhe.h>
#include <NTL/ZZ_pEX.h>
#include <NTL/ZZ_pXFactoring.h>
#include <NTL/ZZ.h>
#include <time.h> 
#include <NTL/vector.h>

using namespace NTL;

class lhe{
    long lwq; 
    Vec<ZZ_pE> gamma;
  public:
    rhe *b;
    lhe();
    Vec<ZZ_pE> BitDecomp(ZZ_pE x);
    Vec<ZZ_pE> PowerOf(ZZ_pE x);
    long SampleMessage();
    void ParamsGen(long t, long n, ZZ q, double delta, long lwq);
    void KeyGen();
    ZZ_pE Encrypt(long m);
    long Decrypt(ZZ_pE c);
    Vec<ZZ_pE> KeySwitchGen();
    Vec<ZZ_pE> GetKeySwitch();
    ZZ_pE KeySwitch(ZZ_pE c);
    ZZ_pE InnerProduct(Vec<ZZ_pE> a, Vec<ZZ_pE> b);
    ZZX Mult(ZZ_pE a, ZZ_pE b, ZZ q);
};

